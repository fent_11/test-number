import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Linking,
    StyleProp,
    TextStyle,
    ViewStyle,
    ScrollView,
    Alert,
    Platform
} from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Header as HeaderRNE, HeaderProps, Icon } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button } from '../component/Button'
import { styles } from '../App'
import { withNavigationFocus } from 'react-navigation'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
    MEMBER
} from '../reduxs'

class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        // console.log(props);
        this.state = {
            memberList: []
        }
    }

    addMember = () => {
        const { navigation } = this.props
        navigation.navigate({ name: 'add' })
    }

    editmember = (index) => {
        const { navigation } = this.props
        navigation.navigate({ name: 'edit', params: { index: index } })
    }
    deletemember = (index) => {
        const { memberReducer, memberAction, navigation } = this.props
        if (Platform.OS === 'web') {
            if (confirm('Are you sure to delete')) {
                memberAction.deleteMember(index, () => {
                    this.init();
                })
            } else {
            }
        } else {
            Alert.alert(
                "Confirmation",
                "Are you sure to delete",
                [
                    {
                        text: "Cancel",
                        style: "cancel"
                    },
                    {
                        text: "OK", onPress: () => {
                            memberAction.deleteMember(index, () => {
                                this.init();
                            })
                        }
                    }
                ]
            );
            console.log(index);
        }


    }

    componentDidMount() {
        this.didFocusSubscription = this.props.navigation.addListener(
            'focus',
            () => {
                this.init();
            }
        );
    }

    init = async () => {
        this.setState({
            memberList: this.props.memberReducer.merberList
        }, () => {
        })
        //fetch some data and set state h

    }
    render() {
        const { memberList } = this.state
        return (
            <ScrollView style={{ padding: 20 }}>
                {memberList.map((elements, index) => {
                    return (
                        <View key={elements.ID} style={styles.memberBox}>
                            <View style={styles.memberBoxLeft}>
                                <Text style={styles.subheaderTextWhite}>
                                    {elements.Name}
                                </Text>
                                <Text style={styles.subheaderTextWhite}>
                                    {elements.ID}
                                </Text>
                                <Text style={styles.subheaderTextWhite}>
                                    {elements.PhoneNumber}
                                </Text>
                            </View>
                            <View style={styles.memberBoxRight}>
                                <TouchableOpacity onPress={() => this.editmember(index)}>
                                    <Icon name='edit' color='white' size={30}></Icon>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.deletemember(index)} style={{ marginTop: 10 }}>
                                    <Icon name='delete' color='red' size={30}></Icon>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                })}
                <View style={styles.container} >
                    {memberList.length == 0 ? <Button onPress={this.addMember} text="Add Your first member" style={styles.buttonFirst} /> :
                        <Button onPress={this.addMember} text="Add member" style={styles.buttonWhite} icon={true} />}
                    <StatusBar style="auto" />
                </View>
            </ScrollView>
        )
    }

}

const mapStateToProps = ({ memberReducer }) => {
    return {
        memberReducer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        memberAction: bindActionCreators(MEMBER.action, dispatch),
    }
}

export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withNavigationFocus(HomeScreen))
