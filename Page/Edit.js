import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Linking,
    StyleProp,
    TextStyle,
    ViewStyle,
    TextInput as Input
} from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Header as HeaderRNE, HeaderProps, Icon } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button } from '../component/Button'
import { styles } from '../App'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
    MEMBER
} from '../reduxs'

class EditScreen extends React.Component {
    constructor(props) {
        super(props)
        console.log(this.props, 'edit');
        let editData = this.props.memberReducer.merberList[this.props.route.params.index]
        this.state = {
            Name: editData.Name,
            ID: editData.ID,
            PhoneNumber: editData.PhoneNumber,
            PhoneNumberInValid: false,
            disabled: false
        }
    }

    onChangeText = (param, value) => {
        if (param == 0) {
            this.setState({
                Name: value
            }, () => {
                this.checkDisable()
            })
        }
        if (param == 1) {
            this.setState({
                ID: value
            }, () => {
                this.checkDisable()
            })
        }
        if (param == 2) {
            var len = value.length;
            if ((len === 3 || len === 7) && len >= this.state.PhoneNumber.length) {
                value = value + "-"
            }
            this.setState({ PhoneNumber: value }, () => {
                this.checkDisable()
            })
            if (this.validate(value)) {
                this.setState({
                    PhoneNumberInValid: false
                })
            } else {

                this.setState({
                    PhoneNumberInValid: true
                })
            }
        }
    }
    checkDisable = () => {
        const { Name, ID, PhoneNumber, disabled } = this.state

        if (Name != '' && ID != '' && PhoneNumber != '') {
            this.setState({
                disabled: false
            })
        } else {
            this.setState({
                disabled: true
            })
        }
    }

    validate = (phone) => {
        const regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        return regex.test(phone)
    }

    addMember = () => {
        const { memberReducer, memberAction, navigation } = this.props
        const { Name, ID, PhoneNumber } = this.state
        let param = {
            Name: Name,
            ID: ID,
            PhoneNumber: PhoneNumber,
            index: this.props.route.params.index
        }
        memberAction.editMember(param, () => {
            navigation.goBack()
        })
    }

    render() {
        const { Name, ID, PhoneNumber, PhoneNumberInValid, disabled } = this.state
        // console.log(this.state, 'state');
        return (<View style={styles.container2}>
            <Input
                style={styles.textInput}
                placeholder={'NAME'}
                onChangeText={(value) => this.onChangeText(0, value)}
                value={Name}
            />
            <Input
                style={styles.textInput}
                placeholder={'ID'}
                keyboardType='numeric'
                onChangeText={(value) => this.onChangeText(1, value)}
                value={ID}
            />
            <Input
                style={styles.textInput}
                placeholder={'PHOMENUMBER'}
                keyboardType='numeric'
                onChangeText={(value) => this.onChangeText(2, value)}
                value={PhoneNumber}
            />
            {PhoneNumberInValid ? <Text style={{ color: 'red' }}> invalid phone number </Text> : null}

            <View style={styles.buttonContainer}>
                <Button onPress={this.addMember} text="Submit" style={styles.buttonSubmit} styleDis={styles.buttonSubmitDisable} disabled={disabled || PhoneNumberInValid} />
            </View>
            {/* <Text>Add Page</Text> */}
        </View>)
    }

}

const mapStateToProps = ({ memberReducer }) => {
    return {
        memberReducer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        memberAction: bindActionCreators(MEMBER.action, dispatch),
    }
}

export default
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(EditScreen)
