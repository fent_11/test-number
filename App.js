import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Linking,
  StyleProp,
  TextStyle,
  ViewStyle,
} from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Header as HeaderRNE, HeaderProps, Icon } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import AddScreen from './Page/Add';
import HomeScreen from './Page/Home';
import EditScreen from './Page/Edit';

import configureStore from './store/configureStore'
import { Provider } from 'react-redux'

const store = configureStore()
console.disableYellowBox = true
const Stack = createNativeStackNavigator()
const headerStyle = {
  backgroundColor: '#397af8',
  color: 'white',
  borderBottomColor: 'white'
}
const headerTintColor = 'white';
const headerTitleStyle = {
  color: 'white',
  fontWeight: '400'
}

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      header: 'My Members',
      memberList: [],
      handlePage: 0 // 0 main , 1 add, 2 edit
    }
  }

  render() {
    const { memberList, handlePage } = this.state
    return (
      <Provider store={store}>
        <SafeAreaProvider>
          <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
              <Stack.Screen name='home' component={HomeScreen}
                options={{
                  title: 'My Members',
                  headerStyle: headerStyle,
                  headerTintColor: headerTintColor,
                  headerTitleStyle: headerTitleStyle,
                }} />
              <Stack.Screen name='add' component={AddScreen}
                options={{
                  title: 'Add new member',
                  headerStyle: headerStyle,
                  headerTintColor: headerTintColor,
                  headerTitleStyle: headerTitleStyle,
                }} />
                <Stack.Screen name='edit' component={EditScreen}
                options={{
                  title: 'Edit member',
                  headerStyle: headerStyle,
                  headerTintColor: headerTintColor,
                  headerTitleStyle: headerTitleStyle,
                }} />
            </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaProvider>
      </Provider>

    );
  }

}

export const styles = StyleSheet.create({
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#397af8',
    marginBottom: 20,
    width: '100%',
    paddingVertical: 15,
  },
  heading: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
  },
  headerRight: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 5,
  },
  subheaderText: {
    color: '#397af8',
    fontSize: 16,
    fontWeight: '400',
    textAlignVertical: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex'
  },
  subheaderTextWhite: {
    color: 'white',
    fontSize: 16,
    fontWeight: '400',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container2: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    padding: 20
  },
  button: {
    flex: 1,
    backgroundColor: '#397af8',
    padding: 15,
    borderRadius: 5,
    width: '100%'
  },
  buttonFirst: {
    marginTop: '100%',
    flex: 1,
    backgroundColor: '#397af8',
    padding: 15,
    borderRadius: 5,
    width: '100%'
  },
  buttonWhite: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderColor: '#397af8',
    color: '#397af8',
    padding: 15,
    borderRadius: 5,
    width: '100%'
  },
  buttonSubmit: {
    flex: 1,
    backgroundColor: '#397af8',
    padding: 15,
    borderRadius: 5,
    width: 200,
    textAlign: 'center'
  },
  buttonSubmitDisable: {
    flex: 1,
    backgroundColor: '#88898C',
    padding: 15,
    borderRadius: 5,
    width: 200,
    textAlign: 'center'
  },
  textInput: {
    height: 50,
    width: '100%'
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    display: 'flex',
    width: '100%'
  },
  memberBox: {
    backgroundColor: '#397af8',
    width: '100%',
    marginBottom: 20,
    borderRadius: 5,
    padding: 20,
    display: 'flex',
    flexDirection: 'row'
  },
  memberBoxRight: {
    flex: 1,
    display: 'flex',
    alignItems: 'flex-end'
  }, 
  memberBoxLeft: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start'
  }
});
