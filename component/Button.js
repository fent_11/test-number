import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import {
    StyleSheet,
    View,
    Text
} from 'react-native';
import { styles } from '../App'
export const Button: React.FunctionComponent<any> = (props) => {
    return (
        <TouchableOpacity
            style={props.disabled ? props.styleDis : props.style}
            onPress={props.onPress}
            disabled={props.disabled}
        >
            <Text style={!props.icon ? styles.subheaderTextWhite : styles.subheaderText}>
                {props.icon ? <Icon name='add' color='#397af8'></Icon> : null}
                {props.text}
            </Text>
        </TouchableOpacity>
    );
}