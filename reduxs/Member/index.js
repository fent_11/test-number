import * as action from './memberAction'
import { default as actionType } from './memberActionType'
import { default as reducer } from './memberReducer'

export const MEMBER = {
  action,
  actionType,
  reducer
}
