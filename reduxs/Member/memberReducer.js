import {
  MEMBER
} from '../../reduxs'

const initialState = {
  merberList: []
  // merberList: [{
  //   Name: 'test1',
  //   ID: '1',
  //   PhoneNumber: '095-921-2772'
  // }]
}

export default (state = initialState, action) => {
  console.log("action", action, state)
  switch (action.type) {
    case MEMBER.actionType.SAVE_MEMBER:
      return {
        merberList: state.merberList
      }
    case MEMBER.actionType.EDIT_MEMBER:
      return {
        ...state,
        merberList: state.merberList.map((item,index) => action.param.index === index ? { ...action.param } : item)
      }
    case MEMBER.actionType.DELETE_MEMBER:
      return {
        merberList: [
          ...state.merberList.slice(0, action.index),
          ...state.merberList.slice(action.index + 1)
        ],
      }
    default:
      return initialState
  }
}
