

import {
  MEMBER
} from '../../reduxs'

//save
export const saveMember = (param, func = () => {}) => {
  return dispatch => {
    dispatch(saveMemberState(param))
    func()
  }
}

export const saveMemberState = (param) => {
  return {
    type: MEMBER.actionType.SAVE_MEMBER,
    param
  }
}

//edit
export const editMember = (param, func = () => {}) => {
  return dispatch => {
    dispatch(editMemberState(param))
    func()
  }
}

export const editMemberState = (param) => {
  return {
    type: MEMBER.actionType.EDIT_MEMBER,
    param
  }
}

//delete
export const deleteMember = (index, func = () => {}) => {
  return dispatch => {
    dispatch(deleteMemberState(index))
    func()
  }
}

export const deleteMemberState = (index) => {
  return {
    type: MEMBER.actionType.DELETE_MEMBER,
    index
  }
}
