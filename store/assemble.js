import {
  combineReducers
} from 'redux'
import * as dataReducer from '../reduxs'
const obj = {}

for (const key in dataReducer) {
  obj[`${key.toLowerCase()}Reducer`] = dataReducer[key].reducer
}
export default combineReducers(obj)
